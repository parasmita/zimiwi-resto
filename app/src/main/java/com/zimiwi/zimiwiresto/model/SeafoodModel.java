package com.zimiwi.zimiwiresto.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeafoodModel {

    @SerializedName("meals")
    @Expose
    private List<MealSeafood> meals = null;

    public List<MealSeafood> getMeals() {
        return meals;
    }

    public void setMeals(List<MealSeafood> meals) {
        this.meals = meals;
    }

}