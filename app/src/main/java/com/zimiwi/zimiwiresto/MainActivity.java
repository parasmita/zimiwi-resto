package com.zimiwi.zimiwiresto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.zimiwi.zimiwiresto.model.MealSeafood;
import com.zimiwi.zimiwiresto.model.SeafoodModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<MealSeafood> meals = new ArrayList<>();
    private RecyclerView.Adapter adapter;
    private String TAG = MainActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rv_heroes);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        LoadJson();



    }

    public void LoadJson(){
        Interface apiInterfacenya = Api.getApiClient().create(Interface.class);
        Call<SeafoodModel> call;
        call = apiInterfacenya.getSeafood("Seafood");
        call.enqueue(new Callback<SeafoodModel>() {
            @Override
            public void onResponse(Call<SeafoodModel> call, Response<SeafoodModel> response) {
                if(response.isSuccessful() && response.body().getMeals() != null){

                    if(!meals.isEmpty()){
                        meals.clear();
                    }
                    meals = response.body().getMeals();
                    adapter = new ZimiwiAdapter(meals, MainActivity.this);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();



                }else {
                    Toast.makeText(MainActivity.this, "No Result!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SeafoodModel> call, Throwable t) {

            }
        });
    }
}
