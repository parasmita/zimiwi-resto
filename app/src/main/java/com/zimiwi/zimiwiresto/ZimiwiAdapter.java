package com.zimiwi.zimiwiresto;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.zimiwi.zimiwiresto.model.MealSeafood;


import java.util.List;

import okhttp3.internal.Util;

public class ZimiwiAdapter extends RecyclerView.Adapter<ZimiwiAdapter.MyViewHolder> implements View.OnClickListener {
    private List<MealSeafood> meals;
    private Context context;

    public ZimiwiAdapter(List<MealSeafood> meals, Context context) {
        this.meals = meals;
        this.context = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_row_zimiwi, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holders, int position) {
        final MyViewHolder holder = holders;
        MealSeafood model = meals.get(position);

        Glide.with(context)
                .load(model.getStrMealThumb())

                .into(holder.imgFood);
        holder.nameFood.setText(model.getStrMeal());
        holder.parentLayout.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return meals.size();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(context, DetailActivity.class);
        context.startActivity(intent);
    }


    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView nameFood;
        ImageView imgFood;
        LinearLayout parentLayout;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.layout_parent);

            nameFood = itemView.findViewById(R.id.tv_item_name);
            imgFood = itemView.findViewById(R.id.img_item_photo);

        }


    }
}
