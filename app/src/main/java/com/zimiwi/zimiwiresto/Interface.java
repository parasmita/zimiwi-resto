package com.zimiwi.zimiwiresto;

import com.zimiwi.zimiwiresto.model.SeafoodModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Interface {
    @GET("filter.php")
    Call<SeafoodModel> getSeafood(
            @Query("c") String country
    );

}
